This repository contains scripts for the long term monitoring of the triplets.

Clone the repository
<pre>
git clone https://gitlab.cern.ch/berkeleylab/labremote-apps/tripletspchainmon.git
</pre>

You then need to run the setup script:
<pre>
source setup.sh
</pre>

The setup script:
- clones `labRemote` and, more specifically, the `devel` branch. 
- creates build directory
- cmake and make labRemote and any other directories

If you make any changes to labRemote or C programs, to implement these changes:
<pre>
cd build
cmake3 ..
make -j4
</pre>