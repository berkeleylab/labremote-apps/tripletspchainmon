#include "TextSerialCom.h"
#include "Logger.h"
#include <stdlib.h>

#include "EquipConf.h"
#include "IPowerSupply.h"
#include "DataSinkConf.h"
#include "IDataSink.h"

#include <iostream>
#include <getopt.h>
#include <chrono>
#include <thread>
#include <sstream>
#include <iomanip>
#include <cstdint>
#include <string>
#include <math.h>
#include <fstream>

#include <nlohmann/json.hpp>
using json = nlohmann::json;
using namespace std;

// variable for input configuration
std::string setupConf;
int psidx;
std::string measurement;
std::string name;
int channel = 1;
std::string configFile;
std::string channelName ="";
std::string sinkName;

void usage(char *argv[])
{
  std::cerr << "" << std::endl;
  std::cerr << "Usage: " << argv[0] << " [options]" << std::endl;
  std::cerr << "List of options:" << std::endl; 
  std::cerr << " -s, --setupConf   Set the input configuration file specifying the arduino port and pin"<< std::endl;  
  std::cerr << " -p, --powerSupply Select which HV PS tot run scan over" << std::endl; 
  std::cerr << " -m, --measurement Sink measurement name" << std::endl;
  std::cerr << " -d, --debug       Enable more verbose printout, use multiple for increased debug level"  << std::endl;
  std::cerr << " -h, --help        List the commands and options available"  << std::endl;
  std::cerr << "" << std::endl;
}


int main(int argc, char* argv[]) 
{
  // Not fully sure why a long sleep here is needed, but
  // without it the code hangs the first time the Arduino
  // is accessed after uploading a sketch
  //std::this_thread::sleep_for(std::chrono::seconds(5));
  
  //Parse command-line  
  if (argc < 1)
  {
    usage(argv);
    return 1;
   }
  
  int c;
  while (true) {
    int option_index = 0;
    static struct option long_options[] = {
      {"setupConf", required_argument, 0,  's' },
      {"powerSupply", required_argument, 0, 'p'},
      {"measurement", required_argument, 0, 'm'},
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {0,          0,                 0,  0 }
    };
                                                                                
    c = getopt_long(argc, argv, "s:p:m:dh", long_options, &option_index);
    if (c == -1)  break;
    switch (c) {
    case 's':
      setupConf = optarg;
      break;
    case 'p':
       psidx = std::stof(optarg);
       break;
    case 'm':
      measurement = optarg;
      break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	std::cerr << std::endl;
	usage(argv);
	return 1;
      }
  }

  // open setup json file 
  std::ifstream ifs(setupConf);
  json js = json::parse(ifs);

  //check if config file is provided, if not create JSON config
  EquipConf hw;
  configFile = js["equipConf"];
  name = js["HV"]["name"][psidx];
  channelName = js["HV"]["channel"][psidx];
  
  if (configFile.empty())
    {
      logger(logERROR) << "No input config file, either create default or use --equip.";
      return 1;
    }
  else
    {
      hw.setHardwareConfig(configFile);
    }

  std::shared_ptr<PowerSupplyChannel> PS;

  if(channelName.empty())
    {
      std::shared_ptr<IPowerSupply> PSreal = hw.getPowerSupply(name);
      PS = std::make_shared<PowerSupplyChannel>(name+std::to_string(channel), PSreal, channel);
    }
  else
    {
      PS = hw.getPowerSupplyChannel(channelName);
    }

  string sinkName = js["sink"]["name"];
  sinkName = "File";
  // set up sink and start measurement
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> sink = ds.getDataSink(sinkName);
  
  sink->setTag("Hardware", channelName);
  
  sink->startMeasurement(measurement, std::chrono::system_clock::now());

  PS->turnOn();
  int i = 1;
  while (i <= 100){
    PS->setVoltageLevel(i);
    PS->setCurrentProtect(0.00001);
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));

    sink->setField("Voltage", PS->measureVoltage());
    
    std::this_thread::sleep_for(std::chrono::seconds(1));

    sink->setField("Current", PS->measureCurrent()*1000000.0);
   
    sink->recordPoint();
    
    if (PS->measureCurrent()*1000000.0 >= 10.0) break;
    
    if (i < 20) i++;
    else i = i + 5;

  }
  PS->turnOff();

  sink->endMeasurement();

  return 0;
}
