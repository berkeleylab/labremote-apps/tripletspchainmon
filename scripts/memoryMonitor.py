#!/usr/bin/python3

import psutil

try:
    from labRemote import ec, datasink
except:
    from _labRemote import ec, datasink

import os, json, time, datetime

home = os.path.expanduser('~')
configFile = home+'/tripletspchainmon/conf/pixelLab-hw.json'
sinkName = 'influxDB'

ds=ec.DataSinkConf()
ds.setHardwareConfig(configFile)
sink = ds.getDataSink(sinkName);

mem = psutil.virtual_memory().percent
    
sink.startMeasurement("tripletSPchain", datetime.datetime.now())
sink.setField("memory_usage", mem)
sink.recordPoint()
sink.endMeasurement()

    



