#!/usr/bin/env python3

import json
import glob
from pylab import *
import numpy as np
import matplotlib.pyplot as plt
import sys
import datetime
from datetime import datetime
import os, time
import os.path
from os import path

import _labRemote as labRemote
from _labRemote import ec,datasink

# get PCIexpress location and triplet name
location='bottom'
triplet='rd53a_3dmodule'
jsetup='setup.json'

# scans to run
scans = ['digitalscan','analogscan', 'thresholdscan', 'noisescan']
FE = ['std', 'diff', 'lin', 'syn']

# list of paths needed
home = os.path.expanduser('~')
currpath = home+'/tripletspchainmon'
outdir = currpath+'/output/'
logdir = currpath+'/log/'
yarrdir = currpath+'/../Yarr/'
yarrconfigdir = currpath+'/conf/3dmodule/'

#configuration file where the equipment is defined
f = open(currpath+'/conf/'+jsetup, 'r')
datastore = json.load(f)
configFile=datastore['equipConf']

fps = open(configFile, 'r')
ds = json.load(fps)

# get HV PS
PSname = datastore['HV']['name']
PSchannel = datastore['HV']['channel']

# set up PS
hw = labRemote.ec.EquipConf()
hw.setHardwareConfig(configFile)

ps = hw.getPowerSupplyChannel(PSchannel)


ps.turnOff()
