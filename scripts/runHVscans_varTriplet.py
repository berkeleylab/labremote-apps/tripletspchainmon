#!/usr/bin/python3                                                                                                                                          
import os, time, sys

mainTriplet = sys.argv[1]#'A'
otherTriplet = []#['B', 'C']
otherTriplet.append(sys.argv[2])
otherTriplet.append(sys.argv[3])
otherTriplet.append(sys.argv[3])
order = 'orderBAC'
#order = 'single'
LV = 'off'
print(mainTriplet, otherTriplet)

path = '/home/eresseguie/tripletspchainmon/'
labRemote = '/home/eresseguie/labRemote/build/bin/powersupply'
hv_cmd = path +'build/bin/HV_IV'
conf = path + 'conf/setup.json'

voltage = ['20', '20', '20', '5', '70', '5', '70']
voltage2 = ['20', '5', '70', '20', '20', '5', '70']

#voltage = ['0']
#voltage2 = ['0']

pscmd_dict = {
    'A': ['1',labRemote + ' -n Keithley-pigwhale-3 -c high-voltage-2 -e '+path+'conf/pixelLab-hw.json'],
    'B': ['0',labRemote + ' -n Keithley-pigwhale-2 -c high-voltage -e '+path+'conf/pixelLab-hw.json'],
    'C': ['2',labRemote + ' -n Keithley-pigwhale-4 -c high-voltage-3 -e '+path+'conf/pixelLab-hw.json']
}

#if not(order == 'single'):
#    ps_power_on = ps_cmd + ' power-on'
#    os.system(ps_power_on)

for j in range (0, 5):
        for i in range (0, len(voltage)):
            for l in range (0, 1):
                name = 'HVtriplet'+mainTriplet+'_LV'+LV+'_'+order+'_HV'+otherTriplet[0]+'_'+voltage[i]+'_HV'+otherTriplet[1]+'_'+voltage2[i]+'_'+str(j) 
                print(name)
                if not(order == 'single'):
                    ps_cmd = pscmd_dict.get(otherTriplet[0])[1]
                    ps_power_on = ps_cmd + ' power-on'
                    os.system(ps_power_on)  
                    full_ps_cmd = ps_cmd + ' set-voltage '+ voltage[i] + ' 0.00001'
                    os.system(full_ps_cmd)
                    
                    ps_cmd = pscmd_dict.get(otherTriplet[1])[1]
                    ps_power_on = ps_cmd + ' power-on'
                    os.system(ps_power_on)
                    full_ps_cmd = ps_cmd + ' set-voltage '+ voltage2[i] + ' 0.00001'
                    os.system(full_ps_cmd)

                    time.sleep(2)
                    
                    option = pscmd_dict.get(mainTriplet)[0]
                    hv_full_cmd2 = hv_cmd + ' -s '+ conf + ' -p '+option+' -m ' + name
                    os.system(hv_full_cmd2)
    
if not(order == 'single'):
    ps_cmd = pscmd_dict.get(otherTriplet[0])[1]
    ps_power_off = ps_cmd + ' power-off'
    os.system(ps_power_off)

    ps_cmd = pscmd_dict.get(otherTriplet[1])[1]
    ps_power_off = ps_cmd + ' power-off'
    os.system(ps_power_off)
    
