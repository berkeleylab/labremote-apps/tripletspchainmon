#!/usr/bin/env python3

import json
import glob
from pylab import *
import numpy as np
import matplotlib.pyplot as plt
import sys
import datetime

import _labRemote as labRemote
from _labRemote import ec,datasink

import os, json, time

home = os.path.expanduser('~')
currpath = home+'/tripletspchainmon'
d = currpath+'/output/'

# get triplet and scanName from the user
triplet=sys.argv[1] 
scanName=sys.argv[2]

if scanName.find('analog') > -1 or scanName.find('digital') > -1 :
    outJson = 'OccupancyMap'
elif scanName.find('noise') > -1:
    outJson = 'NoiseOccupancy'
elif scanName.find('threshold') > -1:
    outJson = 'ThresholdMap'
else: outJson = ''

fileName=glob.glob(d+'out_'+triplet+'_'+scanName+'/last_scan/*'+outJson+'*.json')

#configuration file where the sink is defined
configFile=currpath+'/conf/pixelLab-hw.json'
sinkName='influxDB'

for i in range (0, len(fileName)):
    
    chipName=fileName[i].split('last_scan/')[1].split('_'+outJson)[0]
    with open(fileName[i], 'r') as f:
        jscan = json.load(f)
        scanResult=mean(jscan['Data'])
        
        # set up sink and start measurement
        ds_factory=ec.DataSinkConf(configFile)
        sink = ds_factory.getDataSink(sinkName)
  
        sink.setTag("Setup", 'triplet')

        sink.startMeasurement(scanName, datetime.datetime.now())
        sink.setField(chipName, scanResult)
        sink.recordPoint()
        sink.endMeasurement()
