#!/usr/bin/python3                                                                                                                                          
import os, time

mainTriplet = 'B'
otherTriplet = 'A'
order = 'orderCB'
order = 'single'
LV = 'off'

path = '/home/eresseguie/tripletspchainmon/'
labRemote = '/home/eresseguie/tripletspchain/bin/powersupply'
hv_cmd = path +'bin/HV_IV'
conf = path + 'conf/setup.json'

if mainTriplet == 'B': 
    voltage = ['20', '5', '10', '30', '50', '80', '100']
    if otherTriplet == 'A':ps_cmd = labRemote + ' -n Keithley-pigwhale-3 -c high-voltage-2 -e '+path+'conf/pixelLab-hw.json'
    if otherTriplet == 'C':ps_cmd = labRemote + ' -n Keithley-pigwhale-4 -c high-voltage-3 -e '+path+'conf/pixelLab-hw.json'
    hv_full_cmd = hv_cmd + ' -s '+ conf + ' -p 0 -m '
elif mainTriplet == 'A' : 
    voltage=['10', '5', '2', '8']
    #if order == 'orderAB':
    #    ps_cmd = labRemote + ' -n Keithley-pigwhale-3 -c high-voltage-2 -e '+path+'conf/pixelLab-hw.json'
    #    hv_full_cmd = hv_cmd + ' -s '+ conf + ' -p 0 -m '
    #if order == 'orderBA':
    ps_cmd = labRemote + ' -n Keithley-pigwhale-2 -c high-voltage -e '+path+'conf/pixelLab-hw.json'
    hv_full_cmd = hv_cmd + ' -s '+ conf + ' -p 1 -m '
elif mainTriplet == 'C':
    voltage=['10', '5', '2']#, '8']
    hv_full_cmd = hv_cmd + ' -s '+ conf + ' -p 2 -m '
    if otherTriplet == 'A':ps_cmd = labRemote + ' -n Keithley-pigwhale-3 -c high-voltage-2 -e '+path+'conf/pixelLab-hw.json'
    if otherTriplet == 'B':ps_cmd = labRemote + ' -n Keithley-pigwhale-2 -c high-voltage -e '+path+'conf/pixelLab-hw.json'

if (order == 'single'):
    ps_power_on = ps_cmd + ' power-on'
    print(ps_power_on)
    os.system(ps_power_on)

voltage = ['0']
for j in range (0, 5):
    for i in range (0, len(voltage)):
        if not (order == 'single'): name = 'HVtriplet'+mainTriplet+'_LV'+LV+'_'+order+'_HV'+otherTriplet+voltage[i]+'_'+str(j)
        else: name = 'HVtriplet'+mainTriplet+'_LV'+LV+'_single_'+str(j)
        if not(order == 'single'):
            full_ps_cmd = ps_cmd + ' set-voltage '+ voltage[i] + ' 0.00001'
            os.system(full_ps_cmd)
            time.sleep(1)
        hv_full_cmd2 = hv_full_cmd + name
        print(hv_full_cmd2)
        os.system(hv_full_cmd2)
    time.sleep (1)
    
if not(order == 'single'):
    ps_power_off = ps_cmd + ' power-off'
    os.system(ps_power_off)
