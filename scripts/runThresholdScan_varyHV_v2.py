#!/usr/bin/python3                                                                                                                                          
import os, time

mainTriplet = 'A'
otherTriplet = ['B','C']
order = 'ABC'
HV = 'off'

home = '/home/eresseguie/'
path = '/home/eresseguie/tripletspchainmon/'
labRemote = '/home/eresseguie/labRemote/build/bin/powersupply'
hv_cmd = path +'build/bin/HV_IV'
conf = path + 'conf/setup.json'
Yarr= '/home/eresseguie/Yarr/'

pscmd_dict = {
    'A': ['1',labRemote + ' -n Keithley-pigwhale-3 -c high-voltage-2 -e '+path+'conf/pixelLab-hw.json'],
    'B': ['0',labRemote + ' -n Keithley-pigwhale-2 -c high-voltage -e '+path+'conf/pixelLab-hw.json'],
    'C': ['2',labRemote + ' -n Keithley-pigwhale-4 -c high-voltage-3 -e '+path+'conf/pixelLab-hw.json']
}

yarr_dict = { 
    'A': ['specCfg_top.json', 'rd53a_3dmodule.json'],
    'B': ['specCfg_bottom.json', 'rd53a_3dmodule_B.json'],
    'C': ['specCfg_middle.json', 'rd53a_planarmodule_A.json']
}


voltage = ['20', '5', '10', '30', '50', '80', '100']

ps_cmd_0 = pscmd_dict.get(mainTriplet)[1]
ps_cmd_1 = pscmd_dict.get(otherTriplet[0])[1]
ps_cmd_2 = pscmd_dict.get(otherTriplet[1])[1]

if HV == 'on':
    os.system(ps_cmd_0+ ' power-on')
else:
    os.system(ps_cmd_0+ ' power-off')
    
os.system(ps_cmd_1+ ' power-on')
os.system(ps_cmd_2+ ' power-on')

    
controller = yarr_dict.get(mainTriplet)[0]
connectivity = yarr_dict.get(mainTriplet)[1]

yarr_cmd = Yarr+'bin/scanConsole -r '+Yarr+'configs/controller/'+controller + ' -c '+Yarr+'configs/connectivity/'+connectivity + ' -s '+ Yarr+'configs/scans/rd53a/std_thresholdscan.json -o '+home+'3dmodule'+mainTriplet+'_order'+order+'_HV'+HV

print yarr_cmd

time.sleep(5)




for i in range (0, len(voltage)):
    print 'hv set to ', voltage[i]
    os.system(ps_cmd_1+ ' set-voltage '+ voltage[i] + ' 0.00001')
    os.system(ps_cmd_2+ ' set-voltage '+ voltage[i] + ' 0.00001')

    time.sleep(10)
    
    os.system(yarr_cmd)
    time.sleep(2)
    
#if HV == 'off': os.system(ps_cmd_0+ ' power-off')
#os.system(ps_cmd_0+ ' power-off')
#os.system(ps_cmd_0+ ' power-off')
