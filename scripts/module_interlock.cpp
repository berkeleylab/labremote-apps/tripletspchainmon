#include "TextSerialCom.h"
#include "Logger.h"

#include "EquipConf.h"
#include "IPowerSupply.h"
#include "DataSinkConf.h"
#include "IDataSink.h"

#include "PowerSupplyChannel.h"
#include "Keithley2000.h"

#include <iostream>
#include <getopt.h>
#include <chrono>
#include <thread>
#include <sstream>
#include <iomanip>
#include <cstdint>
#include <string>
#include <math.h>
#include <fstream>

#include <nlohmann/json.hpp>
using json = nlohmann::json;
using namespace std;

// variable for input configuration
std::string name; // name of voltmeter
std::string ps; // name of ps
int channel = 1;
std::string configFile;
std::string channelName ="";
std::string sinkName;

// interlock temperature
float temp = 55.0;


void usage(char *argv[])
{
  std::cerr << "" << std::endl;
  std::cerr << "Usage: " << argv[0] << " [options]" << std::endl;
  std::cerr << "List of options:" << std::endl; 
  std::cerr << " -c, --channel Ch      Set PS channel (name or number when used in conjunction with --name) "<< std::endl;
  std::cerr << " -v, --volt voltmeter  Name of the voltmeter from equipment list " << std::endl;
  std::cerr << " -n, --ps PS channel   Name of the power supply from equipment list " << std::endl;
  std::cerr << " -e, --equip           config.json Configuration file with the power supply definition"<< std::endl;
  std::cerr << " -a, --arduino         Set the input configuration file specifying the arduino port and pin"<< std::endl;  
  std::cerr << " -s, --sink            Name of sink" <<std::endl;
  std::cerr << " -d, --debug           Enable more verbose printout, use multiple for increased debug level"  << std::endl;
  std::cerr << " -h, --help            List the commands and options available"  << std::endl;
  std::cerr << "" << std::endl;
}


int main(int argc, char* argv[]) 
{
  // Not fully sure why a long sleep here is needed, but
  // without it the code hangs the first time the Arduino
  // is accessed after uploading a sketch
  //std::this_thread::sleep_for(std::chrono::seconds(5));
  
  //Parse command-line  
  if (argc < 1)
  {
    usage(argv);
    return 1;
   }
  
  int c;
  while (true) {
    int option_index = 0;
    static struct option long_options[] = {
      {"ps",       required_argument, 0,  'n' },
      {"volt",     required_argument, 0,  'v' },
      {"channel",  required_argument, 0,  'c' },
      {"equip",    required_argument, 0,  'e' },
      {"sink",     required_argument, 0, 's'},
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {0,          0,                 0,  0 }
    };
                                                                                
    c = getopt_long(argc, argv, "n:v:c:e:s:dh", long_options, &option_index);
    if (c == -1)  break;
    switch (c) {
    case 'n':
      ps = optarg;
      break;
    case 'v':
      name = optarg;
      break;
    case 'c':
      try
	{
	  channel    = std::stoi(optarg);
	}
      catch(const std::invalid_argument& e)
	{
	  channelName= optarg;
	}
      break;
    case 'e':
      configFile = optarg;
      break;
   
    case 's':
      sinkName = optarg;
      break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	std::cerr << std::endl;
	usage(argv);
	return 1;
      }
  }

  //check if config file is provided, if not create JSON config
  EquipConf hw;
  
  if (configFile.empty())
    {
      logger(logERROR) << "No input config file, either create default or use --equip.";
      return 1;
    }
  else
    {
      hw.setHardwareConfig(configFile);
    }

  std::shared_ptr<PowerSupplyChannel> VMvirt;

  if(channelName.empty())
    {
      std::shared_ptr<IPowerSupply> VMreal = hw.getPowerSupply(name);
      VMvirt = std::make_shared<PowerSupplyChannel>(name+std::to_string(channel), VMreal, channel);
    }
  else
    {
      VMvirt = hw.getPowerSupplyChannel(channelName);
    }

  std::shared_ptr<Keithley2000> VM = std::dynamic_pointer_cast<Keithley2000>(VMvirt->getPowerSupply());

  VM->beepOff();

  // set up sink and start measurement
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> sink = ds.getDataSink(sinkName);
  
  // meaure FE voltage and VM voltage
  VM->selectMUXChannel(1);
  double FEvolt1012 = abs(VM->measureVoltage());
  VM->deselectMUXChannel(1);
  
  //VM->selectMUXChannel(2);
  //double VMvolt = VM->measureVoltage();
  //VM->deselectMUXChannel(2);

  VM->selectMUXChannel(2);
  double FEvolt1011 = abs(VM->measureVoltage());
  VM->deselectMUXChannel(2);

  VM->selectMUXChannel(3);
  double FEvolt1107 = abs(VM->measureVoltage());
  VM->deselectMUXChannel(3);

  //VM->selectMUXChannel(8);
  //double FEvolt1105 = abs(VM->measureVoltage());
  //VM->deselectMUXChannel(8);

  // if FE volt > threshold, shut off PS
  double threshold = 1.8; 
  if (FEvolt1012 > threshold || FEvolt1011 > threshold || FEvolt1107 > threshold){
    std::shared_ptr<PowerSupplyChannel> PS;

    PS = hw.getPowerSupplyChannel(ps);

    PS->turnOff();

  }
  
  sink->setTag("Hardware", "Keithley");

  sink->startMeasurement("3dmodule", std::chrono::system_clock::now());
  sink->setField("3DA", FEvolt1012);
  sink->setField("3DB", FEvolt1011);
  sink->setField("planarA", FEvolt1107);
  //sink->setField("planarB", FEvolt1105);

  // record all measurements and end measurements
  sink->recordPoint();
  sink->endMeasurement();

  return 0;
}
