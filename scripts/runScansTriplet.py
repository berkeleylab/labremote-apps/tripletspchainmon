#!/usr/bin/env python3

import json
import glob
from pylab import *
import numpy as np
import matplotlib.pyplot as plt
import sys
import datetime
from datetime import datetime
import os, time
import os.path
from os import path

import _labRemote as labRemote
from _labRemote import ec,datasink

# get PCIexpress location and triplet name
location=sys.argv[1] 
triplet=sys.argv[2]
jsetup=sys.argv[3]

# scans to run
scans = ['digitalscan','analogscan', 'thresholdscan']

# list of paths needed
home = os.path.expanduser('~')
currpath = home+'/tripletspchainmon'
outdir = currpath+'/output/'
logdir = currpath+'/log/'
yarrdir = currpath+'/../Yarr/'
yarrconfigdir = currpath+'/conf/Yarr/'

#configuration file where the sink is defined
f = open(currpath+'/conf/'+jsetup, 'r')
datastore = json.load(f)
configFile=currpath+'/conf/'+datastore['config']
sinkName=datastore['sink']

for s in scans:

    # before running a scan, check that the previous scan completed successfully
    
    # check if directory exists
    if str(path.isdir(outdir+'out_'+triplet+'_'+s)):

        # check if previous scan has output json file
        lastscan = os.path.realpath(outdir+'out_'+triplet+'_'+s+'/last_scan/')
        
        lastfiles = glob.glob(lastscan+'/*.json')

        if len(lastfiles) > 0: fileExist = True
        else: fileExist = False

    else:
        # if folder does not exist, want to run a scan
        fileExist = True 
    
    #if not(fileExist): continue

    # get start time of scan
    now = datetime.now()
    dt_string = now.strftime("%d-%m-%Y_%H:%M:%S")
    
    # run scan
    cmd = yarrdir+'bin/scanConsole -r '+yarrconfigdir+'controller/specCfg_'+location+'.json -c '+yarrconfigdir+'connectivity/example_rd53a_IndvComChip'+triplet+'.json -s '+yarrdir+'configs/scans/rd53a/std_'+s+'.json -o '+ outdir+'out_'+triplet+'_'+s +' -n 1 > '+logdir+'log_'+s+'_'+triplet+'_'+dt_string+'.out'  
    
    os.system(cmd)
    os.system('sleep 2')

    end = datetime.now()
    delta = (end-now).total_seconds()
    
    # set up sink and start measurement
    ds_factory=ec.DataSinkConf(configFile)
    sink = ds_factory.getDataSink(sinkName)
    sink.setTag("Setup", 'triplet')
    sink.startMeasurement('delta', end)
    sink.setField(triplet+'_'+s, delta)
    sink.recordPoint()
    sink.endMeasurement()
    
    # analyze scan results
    os.system(currpath+'/scripts/analyzeScan.py ' + triplet + ' ' + s)
