#!/usr/bin/python3                                                                                                                                          
import os, time

mainTriplet = 'C'
otherTriplet = 'B'

order = 'CB'
HV = 'on'

home = '/home/eresseguie/'
path = '/home/eresseguie/tripletspchainmon/'
labRemote = '/home/eresseguie/labRemote/build/bin/powersupply'
hv_cmd = path +'build/bin/HV_IV'
conf = path + 'conf/setup.json'
Yarr= '/home/eresseguie/Yarr/'

if order == 'AB':
    if mainTriplet == 'B': controller='specCfg_bottom.json'
    else: controller ='specCfg_top.json' 
elif order == 'BA':
    if mainTriplet == 'A': controller='specCfg_bottom.json'
    else: controller='specCfg_top.json'
elif order == 'BC' or order == 'CB':
    if mainTriplet == 'B':  controller='specCfg_bottom.json'
    else: controller = 'specCfg_middle.json'
if mainTriplet == 'B': 
    voltage = ['20', '5', '10', '30', '50', '80', '100']
    if HV == 'on':
        ps_cmd = labRemote + ' -n Keithley-pigwhale-2 -c high-voltage -e '+path+'conf/pixelLab-hw.json'
        ps_on = ps_cmd + ' set-voltage 10 0.00001'
        os.system(ps_on)
        ps_power_on = ps_cmd + ' power-on'
        os.system(ps_power_on)
    if otherTriplet == 'A': ps_cmd = labRemote + ' -n Keithley-pigwhale-3 -c high-voltage-2 -e '+path+'conf/pixelLab-hw.json'
    if otherTriplet == 'C': ps_cmd = labRemote + ' -n Keithley-pigwhale-4 -c high-voltage-3 -e '+path+'conf/pixelLab-hw.json'
    ps_default_volt =ps_cmd + ' set-voltage 20 0.00001'
    connectivity = 'rd53a_3dmodule_B.json'
else : 
    voltage=['10', '5', '2', '8']
    if HV == 'on':
        if mainTriplet == 'A': ps_cmd = labRemote + ' -n Keithley-pigwhale-3 -c high-voltage-2 -e '+path+'conf/pixelLab-hw.json'
        if mainTriplet == 'C': ps_cmd = labRemote + ' -n Keithley-pigwhale-4 -c high-voltage-3 -e '+path+'conf/pixelLab-hw.json'
        ps_on = ps_cmd + ' set-voltage 20 0.00001'
        os.system(ps_on)
        ps_power_on = ps_cmd + ' power-on'
        os.system(ps_power_on)
    ps_cmd = labRemote + ' -n Keithley-pigwhale-2 -c high-voltage -e '+path+'conf/pixelLab-hw.json'
    ps_default_volt =ps_cmd + ' set-voltage 10 0.00001'
    if mainTriplet == 'A': connectivity='rd53a_3dmodule.json'
    if mainTriplet == 'C':  connectivity='rd53a_3dmodule_C.json'

yarr_cmd = Yarr+'bin/scanConsole -r '+Yarr+'configs/controller/'+controller + ' -c '+Yarr+'configs/connectivity/'+connectivity + ' -s '+ Yarr+'configs/scans/rd53a/std_thresholdscan.json -o '+home+'3dmodule'+mainTriplet+'_order'+order+'_HV'+HV

print yarr_cmd

os.system(ps_default_volt)
ps_power_on = ps_cmd + ' power-on'
os.system(ps_power_on)
time.sleep(5)

for i in range (0, len(voltage)):
    print 'hv set to ', voltage[i]
    full_ps_cmd = ps_cmd + ' set-voltage '+ voltage[i] + ' 0.00001'
    os.system(full_ps_cmd)
    time.sleep(10)

    os.system(yarr_cmd)
    time.sleep(2)
    
ps_power_off = ps_cmd + ' power-off'
os.system(ps_power_off)

