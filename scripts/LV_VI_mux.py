

import _labRemote as labRemote
import argparse
import logging
import pathlib
import sys
import json
import time
from datetime import datetime
import os

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-s",
        "--setupConf",
        default="",
        help="Set the input configuration file",
    )
    parser.add_argument(
        "-d",
        "--debug",
        default="",
        help="Enable more verbose printout, use multiple for increased debug level",
    )
    
    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)
        logging.debug("Debug mode is on")
    
    conf = args.setupConf

    i = 2.2

    YARRDIR='/home/eresseguie/Yarr'
    YarrController = YARRDIR+'/configs/controller/specCfg_top.json'
    YarrConnectivity = YARRDIR+'/configs/connectivity/rd53a_3dmodule.json'
    yarrcmd = YARRDIR+'/bin/scanConsole -r '+YarrController+' -c '+YarrConnectivity+' -s'

    if (i == 4.5): os.system(yarrcmd)
    #os.system(yarrcmd)

    voltReg = 23
    gndReg = 27
    muxCmd = YARRDIR+'/build/bin/itkpix_set_vmux -r '+YarrController+' -c '+YarrConnectivity+' -v '
    voltMUXcmd = 'sed -i \'s/\"MonitorVmonMux\": '+str(gndReg)+'/\"MonitorVmonMux\": '+str(voltReg)+'/g\' '+YARRDIR+'/configs/rd53a_3dmodule_'
    print voltMUXcmd
    #gndMUXcmd = 'sed -i \'s/\"MonitorVmonMux\": '+str(voltReg)+'/\"MonitorVmonMux\": '+str(gndReg)+'/g\' '+YARRDIR+'/configs/rd53a_3dmodule_'
    
    FEchips= ['A', 'B', 'C']

    with open(conf, 'r') as f:
        js = json.load(f)
        configFile = js["equipConf"]
        name = js["PS"]["name"]
        channelName = js["PS"]["channel"]

        # LV PS
        hw = labRemote.ec.EquipConf()
        hw.setHardwareConfig(configFile)
        PS = hw.getPowerSupplyChannel(channelName)

        # Keithley 2000 voltmeter
        VMreal = hw.getPowerSupply("Keithley-pigwhale")
        VMvirt = labRemote.ps.PowerSupplyChannel('1', VMreal, 1)
        VM = VMvirt.getPowerSupply()

        VM.beepOff()
        
        # setup datasink
        sinkName = js["sink"]["name"]
        sinkName = "File"
        
        ds = labRemote.ec.DataSinkConf()
        ds.setHardwareConfig(configFile)
        sink = ds.getDataSink(sinkName)
        sink.setTag("Hardware", channelName)
        sink.startMeasurement(js["sink"]["measurement"], datetime.now())

        # turn on PS and start measurement
        PS.turnOn()
        
        while (i > 0.1):
            PS.setVoltageProtect(2.3)
            PS.setCurrentLevel(i)
            time.sleep(3)
        
            sink.setField("Current", PS.measureCurrent())

            # set vmux to 24 and read MUX
            newmuxcmd = muxCmd + str(voltReg)
            os.system(newmuxcmd)
            time.sleep(1)

            VM.selectMUXChannel(4)
            time.sleep(1)
            voltA = abs(VM.measureVoltage())
        
            VM.selectMUXChannel(5)
            time.sleep(1)
            voltB = abs(VM.measureVoltage())

            VM.selectMUXChannel(6)
            time.sleep(1)
            voltC = abs(VM.measureVoltage())
            print(voltA, voltB, voltC)
            # set vmux to gnd and read MUX

            newmuxcmd = muxCmd +'27'
            os.system(newmuxcmd)
            time.sleep(1)

            VM.selectMUXChannel(4)
            time.sleep(1)
            voltA = (voltA - abs(VM.measureVoltage()))
            
            VM.selectMUXChannel(5)
            time.sleep(1)
            voltB = (voltB - abs(VM.measureVoltage()))

            VM.selectMUXChannel(6)
            time.sleep(1)
            voltC = (voltC -abs(VM.measureVoltage()))
            print(voltA, voltB, voltC)
            sink.setField("VoltageC", voltC);
            sink.setField("VoltageB", voltB);
            sink.setField("VoltageA", voltA);
            sink.recordPoint()

            i = i -0.1
        
        PS.turnOff()
        
        sink.endMeasurement()
        PS.setVoltageProtect(1.9)
        PS.setCurrentLevel(3.3)
