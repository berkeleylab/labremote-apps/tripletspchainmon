#include "TextSerialCom.h"
#include "Logger.h"
#include <stdlib.h>

#include "EquipConf.h"
#include "IPowerSupply.h"
#include "DataSinkConf.h"
#include "IDataSink.h"
#include "Keithley2000.h"

#include <iostream>
#include <getopt.h>
#include <chrono>
#include <thread>
#include <sstream>
#include <iomanip>
#include <cstdint>
#include <string>
#include <math.h>
#include <fstream>

#include <nlohmann/json.hpp>
using json = nlohmann::json;
using namespace std;

// variable for input configuration
std::string setupConf;
std::string name;
int channel = 1;
std::string configFile;
std::string channelName ="";
std::string sinkName;

void usage(char *argv[])
{
  std::cerr << "" << std::endl;
  std::cerr << "Usage: " << argv[0] << " [options]" << std::endl;
  std::cerr << "List of options:" << std::endl; 
  std::cerr << " -s, --setupConf   Set the input configuration file specifying the arduino port and pin"<< std::endl;  
  std::cerr << " -d, --debug       Enable more verbose printout, use multiple for increased debug level"  << std::endl;
  std::cerr << " -h, --help        List the commands and options available"  << std::endl;
  std::cerr << "" << std::endl;
}


int main(int argc, char* argv[]) 
{
  // Not fully sure why a long sleep here is needed, but
  // without it the code hangs the first time the Arduino
  // is accessed after uploading a sketch
  //std::this_thread::sleep_for(std::chrono::seconds(5));
  
  //Parse command-line  
  if (argc < 1)
  {
    usage(argv);
    return 1;
   }
  
  int c;
  while (true) {
    int option_index = 0;
    static struct option long_options[] = {
      {"setupConf", required_argument, 0,  's' },
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {0,          0,                 0,  0 }
    };
                                                                                
    c = getopt_long(argc, argv, "s:dh", long_options, &option_index);
    if (c == -1)  break;
    switch (c) {
    case 's':
      setupConf = optarg;
      break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	std::cerr << std::endl;
	usage(argv);
	return 1;
      }
  }

  // open setup json file 
  std::ifstream ifs(setupConf);
  json js = json::parse(ifs);

  //check if config file is provided, if not create JSON config
  EquipConf hw;
  configFile = js["equipConf"];
  name = js["PS"]["name"];
  channelName = js["PS"]["channel"];
  
  if (configFile.empty())
    {
      logger(logERROR) << "No input config file, either create default or use --equip.";
      return 1;
    }
  else
    {
      hw.setHardwareConfig(configFile);
    }
  // LV PS
  std::shared_ptr<PowerSupplyChannel> PS;

  if(channelName.empty())
    {
      std::shared_ptr<IPowerSupply> PSreal = hw.getPowerSupply(name);
      PS = std::make_shared<PowerSupplyChannel>(name+std::to_string(channel), PSreal, channel);
    }
  else
    {
      PS = hw.getPowerSupplyChannel(channelName);
    }

  // Keithley voltmeter
  std::shared_ptr<PowerSupplyChannel> VMvirt;
  std::shared_ptr<IPowerSupply> VMreal = hw.getPowerSupply("Keithley-pigwhale");
  VMvirt = std::make_shared<PowerSupplyChannel>(name+std::to_string(channel), VMreal, channel);

  std::shared_ptr<Keithley2000> VM = std::dynamic_pointer_cast<Keithley2000>(VMvirt->getPowerSupply());

  VM->beepOff();
  // select which channel to measure
  VM->selectMUXChannel(6);

  string sinkName = js["sink"]["name"];
  sinkName = "File";
  // set up sink and start measurement
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> sink = ds.getDataSink(sinkName);
  
  sink->setTag("Hardware", channelName);
  
  sink->startMeasurement(js["sink"]["measurement"], std::chrono::system_clock::now());

  PS->turnOn();

  double i = 4.5;
  while(i > 0.1){
    PS->setVoltageProtect(2.3);
    PS->setCurrentLevel(i);
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));
 
    sink->setField("Voltage", abs(VM->measureVoltage()));
    sink->setField("Current", PS->measureCurrent());
   
    sink->recordPoint();
    i = i - 0.1;
  }

  PS->turnOff();

  sink->endMeasurement();

  PS->setVoltageProtect(1.9);
  PS->setCurrentLevel(3.3);

  return 0;
}
