#!/usr/bin/python3                                                                                                                                   

try:
    #from labRemote import ec, datasink
    import labRemote
except:
    #from _labRemote import ec, datasink
    from _labRemote import ec, datasink, com, devcom

import os, json, time, datetime, argparse

# Reference temperature in Kelvin
refTemp=298.15;
# Resistance at reference temperature in ohms
Rref=10000.0;
# Resistance of resistor in voltage divider
Rvdiv=10000.0;
# B value for NTC in Kelvin
Bntc=3435;
# Voltage powering the NTC
voltage=5.0;

# variable for input configuration
channel = 1;

# interlock temperature
interlocktemp = 45.0;

# list of paths needed                                                                                                                               
home = os.path.expanduser('~')
currpath = home+'/tripletspchainmon'

def main(setup):
    time.sleep(5)

    # read from configuration json
    f = open(currpath+'/conf/'+setup, 'r')
    datastore = json.load(f)
    configFile = currpath+'/conf/'+datastore['config']
    sinkName = datastore['sink']
    ntcjson = currpath+'/conf/'+datastore['ntc']
    psName = datastore['PS']
    channel = datastore['PS-channel']

    # get hardware
    hw = ec.EquipConf()
    hw.setHardwareConfig(configFile)
    
    # setup PS
    if not channel:
        psReal = hw.getPowerSupply(psName)
        ps = labRemote.ps.PowerSupplyChannel(f"{args.name}1", psReal, 1)
    else:
        ps = hw.getPowerSupplyChannel(channel)

    # set up sink
    ds=ec.DataSinkConf()
    ds.setHardwareConfig(configFile)
    sink = ds.getDataSink(sinkName);

    sink.startMeasurement("tripletSPchain", datetime.datetime.now())
    sink.setField("Voltage", ps.measureVoltage());
    sink.setField("Current", ps.measureCurrent());

    # read NTC temperature using arduinos
    fntc = open(ntcjson, 'r')
    ntcdata = json.load(fntc)

    portlist = ntcdata['port']
    pinlist = ntcdata['pin']

    for k in range (0,len(portlist)):

        device = portlist[k]
        print(device) 
        c = com.TextSerialCom(device,9600)
        c.setTermination(r"\r\n")
        c.setTimeout(5)
        print(c.termination())
        c.init()

        ntcPins = pinlist[k]
 
        for i in range (0, len(ntcPins)):
            pin = ntcPins[i]

            print(pin)
            cmd = 'ADC '+ str(pin)
            c.send(cmd)
            time.sleep(1)

            try:
                response = c.receive()
            except RuntimeError as e:
                print("Error: "+str(e)+". Continuing")
                time.sleep(1)
                continue

            # convert ADC counts to temperature
            response = float(response)

            pin_voltage=response*voltage/1024.0

            Rntc=Rvdiv*(1024-response)/response

            temp=Bntc*refTemp/(refTemp*log(Rntc/Rref)+Bntc) - 273.15

            # write out temperature to sink
            ntcname = 'NTC'+str(i)+'_Brd'+str(k)
            sink.setField(ntcname, temp)

            # interlock on ntc temperature
            if (temp > interlocktemp):
                # check if PS is on, then turn off PS
                ps_volt = ps.measureVoltage()
            
                if ps_volt > 0.0:
                    ps.turnOff()


    # record all measurements and end measurements
    sink.recordPoint()
    sink.endMeasurement()

        
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('setup', type=str)
    args = parser.parse_args()

    main(args.setup)


