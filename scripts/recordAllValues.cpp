#include "TextSerialCom.h"
#include "Logger.h"

#include "EquipConf.h"
#include "IPowerSupply.h"
#include "DataSinkConf.h"
#include "IDataSink.h"

#include <iostream>
#include <getopt.h>
#include <chrono>
#include <thread>
#include <sstream>
#include <iomanip>
#include <cstdint>
#include <string>
#include <math.h>
#include <fstream>

#include <nlohmann/json.hpp>
using json = nlohmann::json;
using namespace std;

// Reference temperature in Kelvin
float refTemp=298.15;
// Resistance at reference temperature in ohms
float Rref=10000.0;
// Resistance of resistor in voltage divider
float Rvdiv=10000.0;
// B value for NTC in Kelvin
float Bntc=3435;
// Voltage powering the NTC
float voltage=5.0;

// variable for input configuration
std::string setupConf;
std::string name;
int channel = 1;
std::string configFile;
std::string channelName ="";
std::string sinkName;

void usage(char *argv[])
{
  std::cerr << "" << std::endl;
  std::cerr << "Usage: " << argv[0] << " [options]" << std::endl;
  std::cerr << "List of options:" << std::endl; 
  std::cerr << " -s, --setupConf   Set the input configuration file specifying the arduino port and pin"<< std::endl;  
  std::cerr << " -d, --debug       Enable more verbose printout, use multiple for increased debug level"  << std::endl;
  std::cerr << " -h, --help        List the commands and options available"  << std::endl;
  std::cerr << "" << std::endl;
}


int main(int argc, char* argv[]) 
{
  // Not fully sure why a long sleep here is needed, but
  // without it the code hangs the first time the Arduino
  // is accessed after uploading a sketch
  std::this_thread::sleep_for(std::chrono::seconds(5));
  
  //Parse command-line  
  if (argc < 1)
  {
    usage(argv);
    return 1;
   }
  
  int c;
  while (true) {
    int option_index = 0;
    static struct option long_options[] = {
      {"setupConf", required_argument, 0,  's' },
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {0,          0,                 0,  0 }
    };
                                                                                
    c = getopt_long(argc, argv, "s:dh", long_options, &option_index);
    if (c == -1)  break;
    switch (c) {
    case 's':
      setupConf = optarg;
      break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	std::cerr << std::endl;
	usage(argv);
	return 1;
      }
  }

  // open setup json file 
  std::ifstream ifs(setupConf);
  json js = json::parse(ifs);

  //check if config file is provided, if not create JSON config
  EquipConf hw;
  configFile = js["equipConf"];
  name = js["PS"]["name"];
  channelName = js["PS"]["channel"];
  
  if (configFile.empty())
    {
      logger(logERROR) << "No input config file, either create default or use --equip.";
      return 1;
    }
  else
    {
      hw.setHardwareConfig(configFile);
    }

  std::shared_ptr<PowerSupplyChannel> PS;

  if(channelName.empty())
    {
      std::shared_ptr<IPowerSupply> PSreal = hw.getPowerSupply(name);
      PS = std::make_shared<PowerSupplyChannel>(name+std::to_string(channel), PSreal, channel);
    }
  else
    {
      PS = hw.getPowerSupplyChannel(channelName);
    }

  // interlock temperature   
  std::ifstream ifs2(js["ntc"]);
  json j = json::parse(ifs2);

  float temp =j["maxTemp"];

  string sinkName = js["sink"]["name"];

  // set up sink and start measurement
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> sink = ds.getDataSink(sinkName);
  
  sink->setTag("Hardware", channelName);

  sink->startMeasurement(js["sink"]["measurement"], std::chrono::system_clock::now());
  sink->setField("LV Voltage", PS->measureVoltage());
  sink->setField("LV Current", PS->measureCurrent());

  if (!j.contains("port"))
    throw std::runtime_error("No arduino port is provided");

  for (int k=0; k < j["port"].size(); k++){

    logger(logDEBUG) << "Device " << j["port"][k];

    TextSerialCom com(j["port"][k], B9600);
    com.setTermination("\r\n");
    com.init(); 
    logger(logDEBUG) << "initialized";
 
    if (!j.contains("pin"))
      throw std::runtime_error("No pin array is provided");

    for (int i=0; i < j["pin"][k].size(); i ++)
      {

        string ntcName = j["name"][k][i];

	// measure temperature using NTC
	std::string cmd="ADC "; 
	
	int pinNo = j["pin"][k][i];
	
	cmd.append(std::to_string(pinNo));
	com.send(cmd);
	logger(logDEBUG) << "Send command";
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	
	std::string response;
	try
         {
           response = com.receive();
	  }
	catch (std::exception& e)
	  {
	    continue;
          }
       
	std::stringstream ss;
	uint32_t out;
	ss << std::dec << response;
	ss >> out;
            
	float pin_voltage=out*voltage/1024.0;
	logger(logDEBUG1) << "Voltage at pin: " << pin_voltage;

	float Rntc=Rvdiv*(1024-out)/out;
	logger(logDEBUG1) << "NTC Resistance: " << Rntc;

	// Temperature in Celsius
	float ntctemp=Bntc*refTemp/(refTemp*log(Rntc/Rref)+Bntc) - 273.15;
	logger(logDEBUG) << "Temperature: " << ntctemp << " C";     

	// add NTC reading to sink
	sink->setField(ntcName, ntctemp);

      }    
  }
  
  // HV measurements
  nlohmann::json hvPS = js["HV"]["name"];

  for (int i = 0; i < hvPS.size(); i++){

    name = js["HV"]["name"][i];
    channelName = js["HV"]["channel"][i];
    hw.setHardwareConfig(configFile);

    std::shared_ptr<PowerSupplyChannel> PS;

    if(channelName.empty())
      {
	std::shared_ptr<IPowerSupply> PSreal = hw.getPowerSupply(name);
        PS = std::make_shared<PowerSupplyChannel>(name+std::to_string(channel), PSreal, channel);
      }
    else
      {
        PS = hw.getPowerSupplyChannel(channelName);
      }

    sink->setField(channelName+ " Voltage", PS->measureVoltage());
    sink->setField(channelName+ " Current", PS->measureCurrent()); //*1000000.0);                                                                         
  }
  
  // record all measurements and end measurements
  sink->recordPoint();
  sink->endMeasurement();

  return 0;
}
