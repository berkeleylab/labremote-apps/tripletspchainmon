
from pylab import *
import numpy
import matplotlib.pyplot as plt
import os, glob, getopt
import json
from decimal import Decimal

matplotlib.rcParams.update({'font.size': 12}) #sets the font on the plots

colors=['magenta', 'blue', 'green', 'red', 'purple', 'orange']
markers=['.','.', '.', '.', '.', ',']
def superpose(x,y, xtitle, ytitle, title, leg, save, savetype='pdf'):
    fig, ax = plt.subplots()

    # from RD53a manual
    voffset = 0.9

    # use point 3.3A at 1.6 V to calculate slope
    vslope = (1.6 - 0.9) / 3.3
    
    for i in range (0,len(x)):

        ax.plot(x[i], y[i], linestyle = "None", marker=markers[i],markersize=5, color=colors[i])

        # only want to fit above 0.4 A
        newx=np.array(x[i])
        newy=np.array(y[i])
        print newx, newy
        fit_min = 1.4
        fit_max = 1.8

        if leg[i].find('MUX') > -1:
            newx = newx[:len(newy)-6]
            newy = newy[:len(newy)-6]
        else:
            newx = newx[(newy>fit_min)]
            newy = newy[(newy>fit_min)]
        
        if i == 0:
            ax.plot(newx, vslope*newx + voffset, color='black', label='ideal: V = %0.2f * I + %0.2f'%(vslope,voffset))

        m, b = np.polyfit(newx, newy, 1)
        ax.plot(newx, m*newx + b, color=colors[i], label='%s: V = %0.2f * I + %0.2f'%(leg[i],m,b), linestyle='dashed')
    
    ax.set_title(title,fontsize=12)
    ax.set_ylabel(ytitle,fontsize=12)
    ax.set_xlabel(xtitle,fontsize=12)
    ax.grid(False)
    ax.legend(loc='lower right', frameon=False, fontsize='x-small');
    plt.savefig(save+'.'+savetype, bbox_inches = 'tight')
    plt.savefig(save+'.png', bbox_inches = 'tight')
    plt.savefig(save+'.eps', bbox_inches = 'tight')
    
    plt.clf()


def main(argv):
   inputfile = ''
   outputfile = ''
   savetype = ''
   minVolt = None
   maxVolt = None

   opts, args = getopt.getopt(argv,"hi:o:s:e:",["ifile=","ofile="])
   if len(opts) == 0:
      print('Missing input filename and output filname!')
      print('Try analyzeIV.py -i <inputfile> -o <outputfile>')
      print('For full list of options run analyzeIV.py -h')
   for opt, arg in opts:
      if opt == '-h':
         print('analyzeIV.py -i <inputfile> -o <outputfile> -s <savetype>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
         print inputfile
      elif opt in ("-o", "--ofile"):
         outputfile = arg
      elif opt in ("-s", "--savetype"):
          savetype = arg

   #if inputfile == '':
   #   print('Missing input file. Try analyzeIV.py -i <inputfile>')
   #   sys.exit()

   if outputfile == '': outputfile = "test"
   if minVolt == None: minVolt = 1.4
   if savetype == '': savetype = 'pdf'

   list_of_files = glob.glob(inputfile+'*csv')
   print list_of_files
   list_of_files.sort()
   curr, volt=([] for i in range(0,2))
   
   leg = []
   for i in range(0, len(list_of_files)):
       fileName = list_of_files[i]
       fileShort = fileName.split('LV')[1].split('.csv')[0].split('triplet')[1]
       if fileName.find('LVoff') > -1 and fileName.find('15') > -1:
           leg.append('LV off, cooling at 15C')
       elif fileName.find('LVon') > -1 and fileName.find('15') > -1:
           leg.append('LV on, cooling at 15C')
       else:
           #if fileName.find('_mux') > -1:
               #leg.append('triplet '+fileShort.split('_')[0])#+', meas. with MUX')
           if not(fileName.find('_mux') > -1):
               leg.append('triplet '+fileShort+', meas. on adapter card')

       with open(fileName, 'r') as f:
           tmp_voltC = []
           tmp_voltB = []
           tmp_curr = []
           tmp_volt = []
           lines = f.readlines()
           for line in lines:
               if line.find('time') > -1: continue
               words=line.split('\n')[0].split(',')
               
               if fileName.find('mux') > -1:
                   tmp_curr.append(float(words[2]))
                   tmp_volt.append(2*float(words[3]))
                   tmp_voltB.append(2*float(words[4]))
                   tmp_voltC.append(2*float(words[5]))
               else:
                   tmp_curr.append(float(words[3]))
                   tmp_volt.append(float(words[2]))
       volt.append(tmp_volt)
       curr.append(tmp_curr)
       
       if fileName.find('mux') > -1:
           curr.append(tmp_curr)
           curr.append(tmp_curr)
           volt.append(tmp_voltB)
           volt.append(tmp_voltC)
           leg.append('triplet '+fileShort.split('_')[0]+' chip C, meas. with MUX')
           leg.append('triplet '+fileShort.split('_')[0]+' chip B, meas. with MUX')
           leg.append('triplet '+fileShort.split('_')[0]+' chip A, meas. with MUX')
   superpose(curr, volt, 'Current [A]', 'Voltage [V]', 'triplet VI curve',leg, outputfile, savetype)

if __name__ == "__main__":
    main(sys.argv[1:])
