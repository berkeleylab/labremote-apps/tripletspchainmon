import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

def mksp(chips,xold,yold, errold,leg, title, save):
    plt.rcParams["figure.figsize"] = [6.4, 4.8]
    fig, ax = plt.subplots(3,1, sharex=True)
    mpl.rcParams['legend.handlelength'] = 0

    for j in range(0, len(chips)):
        print len(xold), len(yold), len(errold)
        x = [0, 1]
        y = [yold[j], yold[j+3]]
        err = [errold[j], errold[j+3]]


        ax[j].axhline(y=0, color='r', linestyle='--')
        
        marker = '.'
        color = 'teal'
        if j == 0: ax[j].errorbar(x,y,err, marker=marker, ecolor=color, mfc=color,mec=color, linestyle='')
        else: ax[j].errorbar(x,y,err, marker=marker, ecolor=color, mfc=color,mec=color,linestyle='')
        ax[j].set_title(chips[j],fontsize=8)
        plt.xticks(x, xold)

    xmin, xmax = plt.xlim()
    ymin, ymax = plt.ylim()


    #plt.xlim (ymin-2, ymax + 2)
    plt.xlim (-0.5, xmax + 0.5)
    ax[2].set_xlabel('HV of other triplet [V]')
    ax[1].set_ylabel('noise difference [e]')
    ax[0].legend(loc='upper left', frameon=False, numpoints=1, ncol = 2, fontsize='small');
    plt.legend(loc='upper left', frameon=False, fontsize='small', numpoints=1, ncol = 2, bbox_to_anchor=(1, 1.2))
    plt.suptitle(title)
    plt.savefig('../../plots/'+save+'.pdf',bbox_inches = 'tight')
    plt.savefig('../../plots/'+save+'.png',bbox_inches = 'tight')
    plt.savefig('../../plots/'+save+'.eps',bbox_inches = 'tight')
    
def mk1D(chips,x,y, err,leg, title, save):
    plt.rcParams["figure.figsize"] = [6.4, 4.8]
    fig, ax = plt.subplots(3,1, sharex=True)
    mpl.rcParams['legend.handlelength'] = 0

    for i in range (0, len(x)):

        HV = int(leg[i].split('HV =')[1])
        if HV == 0: color = 'black'
        if HV == 2: color = 'red'
        if HV == 5: color = 'green'
        if HV == 8: color = 'gold'
        if HV == 10: color = 'blue'
        if HV == 20: color = 'magenta'
        if HV == 30: color = 'darkviolet'
        if HV == 50: color = 'teal' 
        if HV == 80: color = 'red'
        if HV == 100: color = 'gold'

        if leg[i].find('AB') > -1 or leg[i].find('BC') > -1:
            marker='.'
            color = 'teal'
            if leg[i].find('AB') > -1:order = 'AB'
            elif leg[i].find('BC') > -1:order = 'BC'
        elif leg[i].find('BA') > -1 or leg[i].find('CB') > -1:
            marker='+'
            color = 'darkviolet'
            if leg[i].find('BA') > -1 :order = 'BA'
            elif leg[i].find('CB') > -1: order = 'CB'
        chip = leg[i].split(', ')[1]

        for j in range(0,len(chips)):
            if chip.find(chips[j]) > -1:
                ax[j].axhline(y=0, color='r', linestyle='--')

                if j == 0 and HV == 0: ax[j].errorbar(x[i],y[i],err[i], marker=marker, label = 'order %s'%order, ecolor=color, mfc=color,mec=color)
                else: ax[j].errorbar(x[i],y[i],err[i], marker=marker, ecolor=color, mfc=color,mec=color)
            ax[j].set_title(chips[j],fontsize=8)

    xmin, xmax = plt.xlim()
    ymin, ymax = plt.ylim()
    plt.xlim (ymin-2, ymax + 2)
    plt.xlim (-1, xmax + 1)
    ax[2].set_xlabel('HV of other triplet [V]')
    ax[1].set_ylabel('noise difference [e]')
    ax[0].legend(loc='upper left', frameon=False, numpoints=1, ncol = 2, fontsize='small');
    plt.legend(loc='upper left', frameon=False, fontsize='small', numpoints=1, ncol = 2, bbox_to_anchor=(1, 1.2))
    plt.suptitle(title)
    plt.savefig('../../plots/'+save+'.pdf',bbox_inches = 'tight')
    plt.savefig('../../plots/'+save+'.png',bbox_inches = 'tight')
    plt.savefig('../../plots/'+save+'.eps',bbox_inches = 'tight')
