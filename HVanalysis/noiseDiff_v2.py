import json
import glob
import os
from pylab import *
import numpy as np
import matplotlib.pyplot as plt
import sys
import plot_func

#triplet A or B
# HV on or off
triplet = sys.argv[1]
HV = sys.argv[2]
foldername = sys.argv[3]
#position of each FE
syncFe = 0
linFe = 128
diffFe = 264
end = 400 # number of columns

row = 192 # number of rows

home = '../noise/'
folder ='3dmodule'+triplet+'_order*_HV'+HV

folders = glob.glob(home+'HVtests/'+foldername+'/'+folder)
#files= glob.glob(home+'HVtests/AB/'+folder+'/rd*_std_thresholdscan/')

if triplet == 'A': # middle triplet
    chips=['0x2162', '0x2164', '0x2124']
    if HV == 'off':f_base= '004760'
    else: f_base = '004761'
    leg = ['0','10', '5', '2', '8']
if triplet == 'B': # left triplet
    chips=['0x2142', '0x2151', '0x2144']
    if HV == 'off': f_base= '004758'
    else: f_base = '004759'
    leg = ['0','20', '5', '10', '30','50', '80', '100']
if triplet == 'C': # right triplet
    chips = ['0x1B47', '0x1B4A', '0x1B4B']
    if HV == 'off': f_base= '004766'
    else: f_base =  '004765'
    leg = ['0','10', '5', '2', '8']
    
def mkTripletPlot(h, chips, title, save):
    plt.rcParams["figure.figsize"] = (20,4)

    fig, ax = plt.subplots(1,3)
    im1 = ax[0].imshow(h[0], cmap = 'rainbow' , origin = 'bottom',vmin=-50,vmax=50)
    im2 = ax[1].imshow(h[1], cmap = 'rainbow' , origin = 'bottom', vmin=-50,vmax=50)
    im3 = ax[2].imshow(h[2], cmap = 'rainbow' , origin = 'bottom',vmin=-50,vmax=50)
    ax[1].get_yaxis().set_visible(False)
    ax[2].get_yaxis().set_visible(False)
    #ax1.set_ylim(0, 192)
    #ax2.set_ylim(0, 192)
    #ax3.set_ylim(0, 192)
    #ax1.set_xlim(0, 400)
    #ax2.set_xlim(0, 400)
    #ax3.set_xlim(0, 400)
    cbar= plt.colorbar(im3, fraction=0.023, pad=0.02)
    #plt.clim(-50, 50)
    cbar.set_label('noise [e]', rotation=90)
    ax[1].set_xlabel('Col')
    ax[0].set_ylabel('Row')
    ax[0].set_title(chips[0])
    ax[1].set_title(chips[1])
    ax[2].set_title(chips[2])
    plt.suptitle(title,  y=0.92, fontsize=18)
    plt.savefig('../../plots/'+save+'.pdf',bbox_inches = 'tight')
    plt.savefig('../../plots/'+save+'.png',bbox_inches = 'tight')

    
def mkPlot(h, title, save):
    plt.figure()
    im = plt.imshow(h, cmap = 'rainbow' , origin = 'bottom')
    cbar= plt.colorbar(im, fraction=0.023, pad=0.02)
    plt.clim(-50, 50)
    cbar.set_label('noise [e]', rotation=90)
    plt.xlabel('Col')
    plt.ylabel('Row')
    plt.title(title)
    plt.savefig(save+'.pdf')
    plt.savefig(save+'.png')

dnoisearr=[]
basenoise = []

for j in range (0, len(chips)):
    fileName=    home+'baselineData/'+f_base+'_std_thresholdscan/'+chips[j]+'_NoiseMap-0.json'

    with open(fileName, 'r') as f:
        #load json
        jscan = json.load(f)
        data=jscan['Data']
        basenoise.append(np.array(data))

fout = open('../../plots/'+triplet+'_HV'+HV+'_noiseDiff_otherHV.tex', 'w')
fout2 = open('../../plots/'+triplet+'_HV'+HV+'_noiseDiff_otherHV_nosyn.tex', 'w')

fout.write('\\begin{tabular}{lccc c c} \n')
fout.write('\\hline \n')
fout.write('Order & Chip & HV of other triplet & syn FE $\\Delta$ noise [e] &lin FE $\\Delta$ noise [e] & diff FE $\\Delta$ noise [e] \\\\ \n')
fout.write('\\hline \n')

fout2.write('\\begin{tabular}{l cc c c} \n')
fout2.write('\\hline \n')
fout2.write('Chip & HV of other triplet & lin FE $\\Delta$ noise [e] & diff FE $\\Delta$ noise [e] \\\\ \n')
fout2.write('\\hline \n')

syn_mean_arr=[]
syn_std_arr=[]
lin_mean_arr=[]
lin_std_arr=[]
diff_mean_arr=[]
diff_std_arr=[]
hv_vals=[]
legend =[]

for fol in folders:
    files= glob.glob(fol+'/*_std_thresholdscan/')
    print files
    for i in range(0,len(files)):
        order = files[i].split('order')[1].split('_HV')[0]
        noise= []
    
        for j in range (0, len(chips)):
            fileName= files[i]+chips[j]+'_NoiseMap-0.json'

            with open(fileName, 'r') as f:
                #load json
                jscan = json.load(f)
                data=jscan['Data']

                cols= len(data) #400
                tmp_noise = data

                noise.append(np.array(tmp_noise))

            dnoise = noise[j]-basenoise[j]
    
            # syn FE mean and std
            syn_mean = np.mean (dnoise[syncFe:linFe-1])
            syn_std = np.std (dnoise[syncFe:linFe-1])

            syn_mean_arr.append(syn_mean)
            syn_std_arr.append(syn_std)
            
            # lin FE mean and std
            lin_mean = np.mean (dnoise[linFe:diffFe-1])
            lin_std = np.std (dnoise[linFe:diffFe-1])
            
            lin_mean_arr.append(lin_mean)
            lin_std_arr.append(lin_std)
            
            # diff FE mean and std
            diff_mean = np.mean (dnoise[diffFe:end-1])
            diff_std = np.std (dnoise[diffFe:end-1])

            diff_mean_arr.append(diff_mean)
            diff_std_arr.append(diff_std)
            hv_vals.append(float(leg[i]))
            legend.append('order ' +order+', '+ chips[j]+', HV = '+leg[i])
            
            fout.write(order+'&'+chips[j]+ '& %s'%leg[i] + ' & %0.2f $\\pm$ %0.2f'%(syn_mean, syn_std)+ ' & %0.2f $\\pm$ %0.2f'%( lin_mean, lin_std)+ '& %0.2f $\\pm$ %0.2f \\\\ \n'%( diff_mean, diff_std))
            fout2.write(order+'&'+chips[j]+ '& %s'%leg[i] + ' & %0.2f $\\pm$ %0.2f'%( lin_mean, lin_std)+ '& %0.2f $\\pm$ %0.2f \\\\ \n'%( diff_mean, diff_std))

            dnoise = np.transpose(dnoise)
            dnoisearr.append(dnoise)
        
        #mkTripletPlot(dnoisearr, chips, 'triplet '+triplet + ', HV ' + HV + ', other triplet HV '+ leg[i] +' V', triplet+'_noiseDiff_order'+order+'_HV'+HV+'_otherHV'+leg[i])

plot_func.mk1D(chips,hv_vals,syn_mean_arr, syn_std_arr,legend, 'HV '+HV +', triplet '+triplet+ ', synchronous FE', 'HV'+HV +'_triplet'+triplet+'_order'+foldername+'_synFE')
print len(diff_mean_arr), len(diff_std_arr)
plot_func.mk1D(chips,hv_vals,diff_mean_arr, diff_std_arr,legend, 'HV '+HV +', triplet '+triplet+ ', differential FE', 'HV'+HV +'_triplet'+triplet+'_order'+foldername+'_diffFE')
plot_func.mk1D(chips,hv_vals,lin_mean_arr, lin_std_arr,legend, 'HV '+HV +', triplet '+triplet+ ', linear FE', 'HV'+HV +'_triplet'+triplet+'_order'+foldername+'_linFE')

fout.write('\\hline \n')
fout.write('\\end{tabular}')

fout2.write('\\hline \n')
fout2.write('\\end{tabular}')

fout.close()
fout2.close()
