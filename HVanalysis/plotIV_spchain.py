

from pylab import *
import numpy
import matplotlib.pyplot as plt
import os, glob, getopt
import json
from matplotlib import gridspec

matplotlib.rcParams.update({'font.size': 12}) #sets the font on the plots

colors=['blue', 'green', 'orange', 'darkviolet', 'teal', 'magenta','gold']

minVolt = None
maxVolt = None
LVs=[sys.argv[1]]
order= sys.argv[2]
triplets=[sys.argv[3]]
savetype = 'pdf'
spfolder=['SP_3', 'SP_4']

def superpose(x,y, xtitle='Voltage[V]', ytitle='Current[uA]', title='', save='test',leg=None, savetype='pdf'):
    fig, ax = plt.subplots()
    for i in range (0,len(x)):
        if (leg == None):  
            ax.plot(x[i],y[i], ls='', marker='.', color = 'black')
        else:
            if leg[i].find('A') > -1 : c = 'red'
            if leg[i].find('B') > -1 : c = 'orange'
            if leg[i].find('C') > -1 : c = 'green'
            
            ax.plot(x[i], y[i], linestyle = "None", marker='.',markersize=5, color=c, label='Triplet '+leg[i])

    ax.set_title(title,fontsize=12)
    ax.set_ylabel(ytitle,fontsize=12)
    ax.set_xlabel(xtitle,fontsize=12)
    ax.grid(False)
    if title.find('B') > -1 :ax.legend(loc='upper left', frameon=False, numpoints=1);
    elif title.find('A') > -1 :ax.legend(loc='lower right',frameon=False, numpoints=1)
    else: ax.legend(loc='lower right', frameon=False, numpoints=1)
    plt.savefig(save+'.'+savetype)

    plt.clf()

def ratio(numx,numy,yerr, denomx, denomy, denomerr, xtitle='Voltage[V]', ytitle='Current[uA]', title='', save='test',leg=None, savetype='pdf'):
    fig, ax = plt.subplots(2, 1, sharex=True)#
    gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
    ax[0] = plt.subplot(gs[0])
    ax[1] = plt.subplot(gs[1])
    ax[0].errorbar(denomx[0], denomy[0], yerr=denomerr[0], linestyle = "solid", marker='.',markersize=3, color='black', label='order '+order+', other triplet '+' HV = '+str(HV[0])+'V')

    print denomy
    for i in range (0,len(numx)):
        color = colors[i]
        
        ax[0].errorbar(numx[i], numy[i], yerr=yerr[i], linestyle = "solid", marker='.',markersize=3, color=color, label= leg[i])
    for i in range (0,len(numx)):
        color = colors[i]

        out = np.subtract(numy[i], denomy[0])

        x_min, x_max = ax[0].get_xlim()
        ax[1].hlines(y=0, xmin=x_min, xmax=x_max, linewidth=2, color='r', linestyle = 'dashed')

        if title.find('triplet A') > -1:
            ax[1].hlines(y=-0.08, xmin=x_min, xmax=x_max, linewidth=2, color='r', linestyle = 'dashed')
            ax[1].hlines(y=0.08, xmin=x_min, xmax=x_max, linewidth=2, color='r', linestyle = 'dashed')
        if title.find('triplet B') > -1:
            ax[1].hlines(y=-0.04, xmin=x_min, xmax=x_max, linewidth=2, color='r', linestyle = 'dashed')
            ax[1].hlines(y=0.04, xmin=x_min, xmax=x_max, linewidth=2, color='r', linestyle = 'dashed')
        if title.find('triplet C') > -1:
            ax[1].hlines(y=-0.2, xmin=x_min, xmax=x_max, linewidth=2, color='r', linestyle = 'dashed')
            ax[1].hlines(y=0.2, xmin=x_min, xmax=x_max, linewidth=2, color='r', linestyle = 'dashed')
            
        ax[1].plot(numx[i], out, linestyle = "solid", marker='.',markersize=5, color=color)
        
    ax[0].set_title(title,fontsize=12)
    ax[0].set_ylabel(ytitle,fontsize=12)
    plt.setp(ax[0].get_xticklabels(), visible=False)
    ax[0].grid(False)
    x_min, x_max = ax[0].get_xlim()
    y_min2, y_max2 = ax[0].get_ylim()
    ax[0].set_ylim(y_min2, y_max2*1.2)

    ax[1].set_xlabel(xtitle,fontsize=12)
    ax[1].set_ylabel('X - 2 module chain [uA]')
    ax[1].set_xlim(x_min, x_max)
    y_min, y_max = ax[1].get_ylim()

    if y_min > -0.1 and y_max < 0.10: ax[1].set_ylim(-0.1, 0.1)
    elif y_min > -0.1 and y_max > 0.1: ax[1].set_ylim(-0.1, ymax)
    elif y_min < -0.1 and y_max < 0.1: ax[1].set_ylim(y_min, 0.1)
    else: ax[1].set_ylim(y_min, y_max)
    
    ax[0].legend(loc='upper left', frameon=False, numpoints=1)
    #if title.find('B') > -1 :ax[0].legend(loc='upper left', frameon=False, numpoints=1);
    #elif title.find('A') > -1 :ax[0].legend(loc='upper left',frameon=False, numpoints=1)
    #else: ax[0].legend(loc='lower right', frameon=False, numpoints=1)

    plt.savefig(save+'.'+savetype)
    plt.clf()

    
inputfile = ''# sys.argv[1]
outputfile = 'test' #sys.argv[2]

single_volt_arr, single_curr_arr=([] for i in range(0,2))
for LV in LVs:
    tmp_single_volt_arr, tmp_single_curr_arr=([] for i in range(0,2))
    
    for t in range(0,len(triplets)):
        triplet = triplets[t]

        leg = []
        
        if triplet == 'C': triplet_type = 'planar'
        else: triplet_type = '3D'

        if triplet == 'A' or triplet == 'C': HV= ['20']
        else: HV = ['20']

        if triplet == 'A':
            if order == 'AB': orders=['ABC','ABDC']
            if order == 'BA': orders=['CBA', 'CDBA']

        if triplet == 'C':
            if order == 'CB': orders=['CBA','CDBA']
            if order == 'BC': orders=['ABC', 'ABDC']
        
        orig_curr, orig_volt, orig_curr_arr, orig_volt_arr,orig_curr_err=([] for i in range(0,5))
        new_curr_arr, new_volt_arr, new_curr_err=([] for i in range(0,3))
        
        # baseline file

        if order == 'AB' or order == 'BA' or order =='CB' or order =='BC': tmpHV = '10'
        else: tmpHV = HV[0]
        fbaseline = glob.glob('../SP_2/*triplet'+triplet+'*LV'+LV+'*order'+order+'*HV*'+tmpHV+'*csv')
        print '../SP_2/*triplet'+triplet+'*LV'+LV+'*order'+order+'*HV*'+tmpHV+'*csv'
        for fileName in fbaseline:
            with open(fileName, 'r') as f:
                tmp_curr = []
                tmp_volt = []
                lines = f.readlines()
                for line in lines:
                    if line.find('time') > -1: continue
                    words=line.split('\n')[0].split(',')
                    if float(words[3]) < 20: tmp_curr.append(words[3])
                    else: continue
                    if float(words[2]) < 1:
                        if len(tmp_volt) == 0: tmp_volt.append(1)
                        else: tmp_volt.append(int(tmp_volt[len(tmp_volt)-1])+1)
                    else: tmp_volt.append(words[2])

                if triplet == 'B' and len(tmp_curr) != 29:continue
                elif triplet != 'B' and len(tmp_curr) != 36: continue

                orig_curr.append(np.array(tmp_curr).astype(np.float))
                orig_volt.append(np.array(tmp_volt).astype(np.float))
        orig_volt = np.array(orig_volt)
        orig_curr = np.array(orig_curr)
        orig_volt_arr = np.mean(orig_volt, axis=0)
        orig_curr_arr = np.mean(orig_curr, axis=0)
        orig_curr_err = np.std(orig_curr, axis=0)


        # input new files
        for j in range(0, len(orders)):
            leg.append('order '+orders[j]+', other triplet '+' HV = '+str(HV[0])+'V')
            new_curr, new_volt=([] for i in range(0,2))
            tmp_curr_arr, tmp_volt_arr, tmp_curr_err=([] for i in range(0,3))
            fnew = glob.glob('../'+spfolder[j]+'/*triplet'+triplet+'*LV'+LV+'*order'+orders[j]+'*HV*'+HV[0]+'*csv')
            
            for fileName in fnew:

                with open(fileName, 'r') as f:
                    tmp_curr = []
                    tmp_volt = []
                    lines = f.readlines()
                    for line in lines:
                        if line.find('time') > -1: continue
                        words=line.split('\n')[0].split(',')
                        if float(words[3]) < 20: tmp_curr.append(words[3])
                        else: continue
                        if float(words[2]) < 1:
                            if len(tmp_volt) == 0: tmp_volt.append(1)
                            else:tmp_volt.append(int(tmp_volt[len(tmp_volt)-1])+1)
                        else: tmp_volt.append(words[2])
                        
                    if triplet == 'B':
                        if len(tmp_curr) < 29:continue
                        else:
                            tmp_curr = tmp_curr[0:29]
                            tmp_volt = tmp_volt[0:29]
                    elif len(tmp_curr) != 36: continue
                    new_curr.append(np.array(tmp_curr).astype(np.float))
                    new_volt.append(np.array(tmp_volt).astype(np.float))
            new_volt = np.array(new_volt)
            new_curr = np.array(new_curr)
            tmp_volt_arr = np.mean(new_volt, axis=0)
            tmp_curr_arr = np.mean(new_curr, axis=0)
            tmp_curr_err = np.std(new_curr, axis=0)

            new_volt_arr.append(tmp_volt_arr)
            new_curr_arr.append(tmp_curr_arr)
            new_curr_err.append(tmp_curr_err)

        ratio(new_volt_arr,new_curr_arr,new_curr_err, [orig_volt_arr], [orig_curr_arr], [orig_curr_err], title='Sensor IV curve for '+triplet_type+' triplet '+triplet+', LV '+LV,leg=leg,save='spchain_HVcompare_order'+order+'_'+triplet+'_lv'+LV)
    # end triplet loop
