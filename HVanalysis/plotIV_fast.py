from pylab import *
import numpy
import matplotlib.pyplot as plt
import os, glob, getopt
import json
from matplotlib import gridspec

matplotlib.rcParams.update({'font.size': 12}) #sets the font on the plots

colors=['black','blue', 'green', 'red', 'orange', 'darkviolet', 'teal', 'magenta','gold']
#marker=['.','.', '.', '.', '+', '+']
def superpose(x,y, xtitle='Voltage[V]', ytitle='Current[uA]', title='', save='test',leg=None, savetype='pdf'):
    fig, ax = plt.subplots()
    for i in range (0,len(x)):
        if (leg == None):  
            ax.plot(x[i],y[i], ls='', marker='.', color = 'black')
        else:
            if leg[i].find('A') > -1 : c = 'red'
            if leg[i].find('B') > -1 : c = 'orange'
            if leg[i].find('C') > -1 : c = 'green'
            
            ax.plot(x[i], y[i], linestyle = "None", marker='.',markersize=5, color=c, label='Triplet '+leg[i])

    ax.set_title(title,fontsize=12)
    ax.set_ylabel(ytitle,fontsize=12)
    ax.set_xlabel(xtitle,fontsize=12)
    ax.grid(False)
    if title.find('B') > -1 :ax.legend(loc='upper left', frameon=False, numpoints=1);
    if title.find('A') > -1 :ax.legend(loc='lower right',frameon=False, numpoints=1)
    else: ax.legend(loc='upper left', frameon=False, numpoints=1)
    plt.savefig(save+'.'+savetype)

    plt.clf()

def ratio(numx,numy,yerr, denomx, denomy, denomerr, xtitle='Voltage[V]', ytitle='Current[uA]', title='', save='test',leg=None, savetype='pdf'):
    print len(numx),len(numy)
    fig, ax = plt.subplots(2, 1, sharex=True)#
    gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
    ax[0] = plt.subplot(gs[0])
    ax[1] = plt.subplot(gs[1])

    ax[0].errorbar(denomx[0], denomy[0], yerr=denomerr[0], linestyle = "solid", marker='.',markersize=3, color='black', label='single')
    for i in range (0,len(numx)):
        color = 'blue'
        '''
        if HV == 0: color = 'blue'
        if HV == 2: color = 'green'
        if HV == 5: color = 'red'
        if HV == 8: color = 'orange'
        if HV == 10: color = 'darkviolet'
        if HV == 20: color = 'green'
        if HV == 30: color = 'orange'
        if HV == 50: color = 'teal'
        if HV == 80: color = 'magenta'
        if HV == 100: color = 'gold'
        '''
        ax[0].errorbar(numx[i], numy[i], yerr=yerr[i], linestyle = "solid", marker='.',markersize=3, color=color, label= 'LV disc')
        out = np.subtract(numy[i], denomy[0])

        x_min, x_max = ax[0].get_xlim()

        ax[1].hlines(y=0, xmin=x_min, xmax=x_max, linewidth=2, color='r', linestyle = 'dashed')
        ax[1].hlines(y=-0.08, xmin=x_min, xmax=x_max, linewidth=2, color='r', linestyle = 'dashed')
        ax[1].hlines(y=0.08, xmin=x_min, xmax=x_max, linewidth=2, color='r', linestyle = 'dashed')

        ax[1].plot(numx[i], out, linestyle = "solid", marker='.',markersize=5, color=color)

        
    ax[0].set_title(title,fontsize=12)
    ax[0].set_ylabel(ytitle,fontsize=12)
    plt.setp(ax[0].get_xticklabels(), visible=False)
    ax[0].grid(False)
    x_min, x_max = ax[0].get_xlim()

    ax[1].set_xlabel(xtitle,fontsize=12)
    print xtitle
    ax[1].set_ylabel('chain - single [uA]')
    ax[1].set_xlim(x_min, x_max)
    y_min, y_max = ax[1].get_ylim()
    if y_min > -0.1 and y_max < 0.10: ax[1].set_ylim(-0.1, 0.1)
    elif y_min > -0.1 and y_max > 0.1: ax[1].set_ylim(-0.1, ymax)
    elif ymin < -0.1 and y_max < 0.1: ax[1].set_ylim(y_min, 0.1)

    ax[0].legend(loc='upper left', frameon=False, numpoints=1)

    plt.savefig(save+'.'+savetype)
    plt.clf()

    
inputfile = ''# sys.argv[1]
outputfile = 'test' #sys.argv[2]
minVolt = None
maxVolt = None
LVs=['off']
triplets=['A', 'C']
triplet_type=['3D', 'planar']
savetype = 'pdf'

single_volt_arr, single_curr_arr=([] for i in range(0,2))
for LV in LVs:
    tmp_single_volt_arr, tmp_single_curr_arr=([] for i in range(0,2))
    
    for i in range(0,len(triplets)):
        triplet = triplets[i]
        orig_curr, orig_volt, orig_curr_arr, orig_volt_arr,orig_curr_err=([] for i in range(0,5))
        new_curr, new_volt,new_curr_arr, new_volt_arr, new_curr_err=([] for i in range(0,5))
        
        # baseline file
        fbaseline = glob.glob('../single/*'+triplet+'*'+LV+'*single/*csv')

        for fileName in fbaseline:
            with open(fileName, 'r') as f:
                tmp_curr = []
                tmp_volt = []
                lines = f.readlines()
                for line in lines:
                    if line.find('time') > -1: continue
                    words=line.split('\n')[0].split(',')
                    if float(words[3]) < 20: tmp_curr.append(words[3])
                    else: continue
                    if float(words[2]) < 1: tmp_volt.append(int(tmp_volt[len(tmp_volt)-1])+1)
                    else: tmp_volt.append(words[2])
                
                if len(tmp_curr) != 36: continue
        
                orig_curr.append(np.array(tmp_curr).astype(np.float))
                orig_volt.append(np.array(tmp_volt).astype(np.float))
        orig_volt = np.array(orig_volt)
        orig_curr = np.array(orig_curr)
        orig_volt_arr = np.mean(orig_volt, axis=0)
        orig_curr_arr = np.mean(orig_curr, axis=0)
        orig_curr_err = np.std(orig_curr, axis=0)

        #tmp_single_volt_arr.append(single_volt)
        #tmp_single_curr_arr.append(single_curr)

        # input new files
        fnew = glob.glob('../single/*'+triplet+'*'+LV+'*disc/*csv')

        for fileName in fnew:
            with open(fileName, 'r') as f:
                tmp_curr = []
                tmp_volt = []
                lines = f.readlines()
                for line in lines:
                    if line.find('time') > -1: continue
                    words=line.split('\n')[0].split(',')
                    if float(words[3]) < 20: tmp_curr.append(words[3])
                    else: continue
                    if float(words[2]) < 1: tmp_volt.append(int(tmp_volt[len(tmp_volt)-1])+1)
                    else: tmp_volt.append(words[2])

                if len(tmp_curr) != 36: continue

                new_curr.append(np.array(tmp_curr).astype(np.float))
                new_volt.append(np.array(tmp_volt).astype(np.float))
        new_volt = np.array(new_volt)
        new_curr = np.array(new_curr)
        new_volt_arr = np.mean(new_volt, axis=0)
        new_curr_arr = np.mean(new_curr, axis=0)
        new_curr_err = np.std(new_curr, axis=0)

        ratio([new_volt_arr],[new_curr_arr],[new_curr_err], [orig_volt_arr], [orig_curr_arr], [orig_curr_err], title='Sensor IV curve for '+triplet_type[i]+' triplet '+triplet+', LV '+LV,save='disc_vs_closed_triplet'+triplet+'_lv'+LV)
    # end triplet loop
    
    #single_volt_arr.append(tmp_single_volt_arr)
    #single_curr_arr.append(tmp_single_curr_arr)


    #superpose(tmp_single_volt_arr, tmp_single_curr_arr, leg=triplets, title='LV '+LV)

'''     
   list_of_files = glob.glob(inputfile+'*csv')
   list_of_files.sort()
   
   print list_of_files
   curr, volt=([] for i in range(0,2))
   
   leg = []
   for fileName in list_of_files:
       
       with open(fileName, 'r') as f:
           tmp_curr = []
           tmp_volt = []
           lines = f.readlines()
           for line in lines:
               if line.find('time') > -1: continue
               words=line.split('\n')[0].split(',')
               
               tmp_curr.append(words[3])
               tmp_volt.append(words[2])
       volt.append(tmp_volt)
       curr.append(tmp_curr)
   superpose(volt, curr, 'Voltage [V]', 'Current [uA]', title+' Sensor IV curve',outputfile,leg, savetype)
'''
