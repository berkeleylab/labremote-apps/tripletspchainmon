
from pylab import *
import numpy
import matplotlib.pyplot as plt
import os, glob, getopt
import json

matplotlib.rcParams.update({'font.size': 12}) #sets the font on the plots

colors=['black','blue', 'green', 'red', 'orange', 'darkviolet', 'teal', 'magenta','gold']
#markers=['.','.', '.', '.', '+', '+']
def superpose(x,y, xtitle, ytitle, title, save,leg=None, savetype='pdf'):
    fig, ax = plt.subplots()

    for i in range (0,len(x)):
        if (leg == None):  
            ax.plot(x[i],y[i], ls='', marker='.')
        else:
            #if leg[i].find('A') > -1 : c = 'blue'
            #if leg[i].find('B') > -1 : c = 'red'
            #if leg[i].find('C') > -1 : c = 'green'
            ax.plot(x[i], y[i], linestyle = "None", marker='.',markersize=5, color=colors[i], label=leg[i])

    ax.set_title(title,fontsize=12)
    ax.set_ylabel(ytitle,fontsize=12)
    ax.set_xlabel(xtitle,fontsize=12)
    ax.grid(False)
    if title.find('B') > -1 :ax.legend(loc='upper left', frameon=False, numpoints=1);
    if title.find('A') > -1 :ax.legend(loc='lower right',frameon=False, numpoints=1)
    plt.savefig(save+'.'+savetype)

    plt.clf()


def main(argv):
   inputfile = ''
   outputfile = ''
   savetype = ''
   minVolt = None
   maxVolt = None

   opts, args = getopt.getopt(argv,"hi:o:s:e:",["ifile=","ofile="])
   if len(opts) == 0:
      print('Missing input filename and output filname!')
      print('Try analyzeIV.py -i <inputfile> -o <outputfile>')
      print('For full list of options run analyzeIV.py -h')
   for opt, arg in opts:
      if opt == '-h':
         print('analyzeIV.py -i <inputfile> -o <outputfile> -s <savetype>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
         print inputfile
      elif opt in ("-o", "--ofile"):
         outputfile = arg
      elif opt in ("-s", "--savetype"):
          savetype = arg

   #if inputfile == '':
   #   print('Missing input file. Try analyzeIV.py -i <inputfile>')
   #   sys.exit()

   if outputfile == '': outputfile = "test"
   if minVolt == None: minVolt = 1.4
   if savetype == '': savetype = 'pdf'

   list_of_files = glob.glob(inputfile+'*csv')
   list_of_files.sort()
   
   print list_of_files
   curr, volt=([] for i in range(0,2))
   
   leg = []
   for fileName in list_of_files:
       #if fileName.find('22') > -1 : continue
       #fileShort=fileName.split('.csv')[0].split('/')[1].split('_')[0]
       if fileName.find('LVoff') > -1 and fileName.find('15C') > -1:
           leg.append('LV off, cooling at 15C')
       elif fileName.find('LVoff') > -1 and fileName.find('23C') > -1:
           leg.append('LV off, cooling at 23C')
       elif fileName.find('LVoff') > -1 and fileName.find('26C') > -1:
           leg.append('LV off, cooling at 26C')
       elif fileName.find('LVon') > -1 and fileName.find('15C') > -1:
           leg.append('LV on, cooling at 15C')

       legTxt = ''
       if fileName.find('LVon')>-1: legTxt = 'LV on, '
       elif fileName.find('LVoff')>-1: legTxt = 'LV off, '
       if fileName.find('HVB') > -1 or fileName.find('HVA') > -1:
           if fileName.find('HVB') > -1: hvText = 'triplet B HV = '+fileName.split('LV')[1].split('_')[1].split('HVB')[1].split('.csv')[0]
           elif fileName.find('HVA') > -1: hvText = 'triplet A HV = '+fileName.split('LV')[1].split('_')[1].split('HVA')[1].split('.csv')[0]
       else: hvText = 'single triplet'

       if legTxt == '' : hvText = fileName.split('_')[1].split('.csv')[0]

       if fileName.find('tripletB') > -1: title = 'Triplet B'
       if fileName.find('tripletA') > -1: title = 'Triplet A'

       leg.append(legTxt+ hvText)
       #if fileName.find('off')>-1: leg.append(fileShort + ', LV off')
       #else:leg.append(fileShort + ', LV on')
       #leg.append(fileName.split('.csv')[0].split['/'][1])
       with open(fileName, 'r') as f:
           tmp_curr = []
           tmp_volt = []
           lines = f.readlines()
           for line in lines:
               if line.find('time') > -1: continue
               words=line.split('\n')[0].split(',')
               
               tmp_curr.append(words[3])
               tmp_volt.append(words[2])
       volt.append(tmp_volt)
       curr.append(tmp_curr)
   superpose(volt, curr, 'Voltage [V]', 'Current [uA]', title+' Sensor IV curve',outputfile,leg, savetype)

if __name__ == "__main__":
    main(sys.argv[1:])
