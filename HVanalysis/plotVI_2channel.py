
from pylab import *
import numpy
import matplotlib.pyplot as plt
import os, glob, getopt
import json

matplotlib.rcParams.update({'font.size': 12}) #sets the font on the plots

colors=['blue', 'orange', 'purple', 'red']
markers=['.','.', '+', '+']
def superpose(x,y, xtitle, ytitle, title, leg, save, savetype='pdf'):
    fig, ax = plt.subplots()

    for i in range (0,len(x)):
        ax.plot(x[i], y[i], linestyle = "None", marker=markers[i],markersize=5, color=colors[i])

        # only want to fit above 0.4 A
        newx=np.array(x[i])
        newy=np.array(y[i])

        newy = newy[newx>0.8]
        newx= newx[newx>0.8]
        
        m, b = np.polyfit(newx, newy, 1)
        ax.plot(newx, m*newx + b, color=colors[i], label='%s: V = %0.2f * I + %0.2f'%(leg[i],m,b))

    ax.set_title(title,fontsize=12)
    ax.set_ylabel(ytitle,fontsize=12)
    ax.set_xlabel(xtitle,fontsize=12)
    ax.grid(False)
    ax.legend(loc='upper left', frameon=False, ncol=1, prop={'size': 10});
    plt.savefig(save+'.'+savetype)

    plt.clf()


def main(argv):
   inputfile = ''
   outputfile = ''
   savetype = ''
   minVolt = None
   maxVolt = None

   opts, args = getopt.getopt(argv,"hi:o:s:e:",["ifile=","ofile="])
   if len(opts) == 0:
      print('Missing input filename and output filname!')
      print('Try analyzeIV.py -i <inputfile> -o <outputfile>')
      print('For full list of options run analyzeIV.py -h')
   for opt, arg in opts:
      if opt == '-h':
         print('analyzeIV.py -i <inputfile> -o <outputfile> -s <savetype>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
         print inputfile
      elif opt in ("-o", "--ofile"):
         outputfile = arg
      elif opt in ("-s", "--savetype"):
          savetype = arg

   #if inputfile == '':
   #   print('Missing input file. Try analyzeIV.py -i <inputfile>')
   #   sys.exit()

   if outputfile == '': outputfile = "test"
   if minVolt == None: minVolt = 1.4
   if savetype == '': savetype = 'pdf'

   list_of_files = glob.glob(inputfile+'*.csv')
   print list_of_files
   curr, volt=([] for i in range(0,2))
   
   leg = []
   for fileName in list_of_files:
       #if(fileName.find('nopowercycle') > -1):
       #    leg.append('no power cycle, analog')
       #    leg.append('no power cycle, digital')
       #else:
       #    leg.append('power cycle, analog')
       #    leg.append('power cycle, digital')
       with open(fileName, 'r') as f:
           tmp_anal_curr = []
           tmp_anal_volt = []
           tmp_dig_curr = []
           tmp_dig_volt = []
           lines = f.readlines()
           for line in lines:
               if line.find('time') > -1: continue
               words=line.split('\n')[0].split(',')
               print fileName, line
               tmp_dig_curr.append(float(words[3]))
               tmp_dig_volt.append(float(words[2])-0.5*float(words[3]))
               tmp_anal_curr.append(float(words[5]))
               tmp_anal_volt.append(float(words[4])-0.5*float(words[3]))
       volt.append(tmp_anal_volt)
       volt.append(tmp_dig_volt)
       curr.append(tmp_anal_curr)
       curr.append(tmp_dig_curr)
   superpose(curr, volt, 'Current [A]', 'Voltage [V]', inputfile+' VI curve',leg, outputfile, savetype)

if __name__ == "__main__":
    main(sys.argv[1:])
