import json
import glob
from pylab import *
import numpy as np
import matplotlib.pyplot as plt
import sys

triplet = 'C'
print 'triplet', triplet
#position of each FE
syncFe = 0
linFe = 128
diffFe = 264
end = 400 # number of columns

row = 192 # number of rows

if triplet == 'A': # middle triplet
    chips=['0x2162', '0x2164', '0x2124']
    f_hvoff= '004760'
    f_hvon = '004761'
if triplet == 'B': # left triplet
    chips=['0x2142', '0x2151', '0x2144']
    f_hvoff= '004758'
    f_hvon = '004759'
if triplet == 'C': # right triplet
    chips = ['0x1B47', '0x1B4A', '0x1B4B']
    f_hvoff= '004766'
    f_hvon = '004765'

files= [f_hvoff, f_hvon]

def mk1D(h, title, save):
    plt.figure()
    plt.plot(h)
    plt.xlabel('Col')
    plt.ylabel('Row')
    plt.title(title)
    plt.savefig(save+'.pdf')
    plt.savefig(save+'.png')


def mkTripletPlot(h, chips, title, save):
    fig, axs = plt.subplot(1,3)
    im = axs[0,0].imshow(h, cmap = 'rainbow' , origin = 'bottom')
    cbar= plt.colorbar(im, fraction=0.023, pad=0.02)
    plt.clim(-50, 50)
    cbar.set_label('noise [e]', rotation=90)
    plt.xlabel('Col')
    plt.ylabel('Row')
    plt.title(title)
    plt.savefig(save+'.pdf')
    plt.savefig(save+'.png')

    
def mkPlot(h, title, save):
    plt.figure()
    im = plt.imshow(h, cmap = 'rainbow' , origin = 'bottom')
    cbar= plt.colorbar(im, fraction=0.023, pad=0.02)
    plt.clim(-50, 50)
    cbar.set_label('noise [e]', rotation=90)
    plt.xlabel('Col')
    plt.ylabel('Row')
    plt.title(title)
    plt.savefig(save+'.pdf')
    plt.savefig(save+'.png')

dnoisearr=[]
for j in range (0, len(chips)):
    noise= []
    for i in range(0,len(files)):
        fileName= files[i]+'_std_thresholdscan/'+chips[j]+'_NoiseMap-0.json'

        with open(fileName, 'r') as f:
            #load json
            jscan = json.load(f)
            data=jscan['Data']

            cols= len(data) #400
            tmp_noise = data
            #tmp_noise = np.transpose(tmp_noise)
            noise.append(np.array(tmp_noise))

    dnoise = noise[1]-noise[0]
    
    # syn FE mean and std
    syn_mean = np.mean (dnoise[syncFe:linFe-1])
    syn_std = np.std (dnoise[syncFe:linFe-1])

    # lin FE mean and std
    lin_mean = np.mean (dnoise[linFe:diffFe-1])
    lin_std = np.std (dnoise[linFe:diffFe-1])
    
    # diff FE mean and std
    diff_mean = np.mean (dnoise[diffFe:end-1])
    diff_std = np.std (dnoise[diffFe:end-1])
    
    print chips[j], 'syn: %0.2f \\pm %0.2f'%(syn_mean, syn_std), 'lin: %0.2f \\pm %0.2f'%( lin_mean, lin_std), 'diff:%0.2f \\pm %0.2f'%( diff_mean, diff_std)

    dnoise = np.transpose(dnoise)
    dnoisearr.append(dnoise)
    mkPlot(dnoise, chips[j]+' HV on - HV off noise difference', triplet+'_'+str(j)+'_'+chips[j]+'_noiseDiff')

#mkTripletPlot(dnoisearr, chips, 'triplet '+triplet + ' HV on - HV off noise difference', triplet+'_noiseDiff')
